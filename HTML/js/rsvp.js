jQuery(document).ready(function() {

    var guests = [{
	"firstNames": [
		"Brandon", "Geneva"
	],
	"lastNames": ["Fox", "Allen"]
},
{
"firstNames": [
"Kathryn"
],
"lastNames": ["Stahl"]
}, {
	"firstNames": [
		"Rose", "Gary"
	],
	"lastNames": ["Fox", "Fox"]
}, {
	"firstNames": [
		"Heidi", "Greg", "Cooper", "Hadley"
	],
	"lastNames": ["Matson", "Matson", "Matson", "Matson"]
}, {
	"firstNames": ["Heather"],
	"lastNames": ["Fox"]
}, {
	"firstNames": [
		"Tyler", "Kelli"
	],
	"lastNames": ["Scott", "Scott"]
}, {
	"firstNames": [
		"Connie", "Justin"
	],
	"lastNames": ["Childs", "Fox"]
}, {
	"firstNames": [
		"Randy", "Teresa", "Emma"
	],
	"lastNames": ["Fox", "Fox", "Fox"]
}, {
	"firstNames": [
		"Megan", "Travis"
	],
	"lastNames": ["Hicks", "Hicks"]
}, {
	"firstNames": [
		"Josh", "Taylor"
	],
	"lastNames": ["Bozich", "Bozich"]
}, {
	"firstNames": [
		"McKenzie", "Isaac"
	],
	"lastNames": ["Dent", "Lawrence"]
}, {
	"firstNames": [
		"Mitch", "Kirsten"
	],
	"lastNames": ["McClain", "Silver"]
}, {
	"firstNames": ["Dana"],
	"lastNames": ["Brown"]
}, {
	"firstNames": ["Rutvi"],
	"lastNames": ["Patel"]
}, {
	"firstNames": [
		"Ben", "Jess", "Josiah", "Hudson"
	],
	"lastNames": ["Sanders", "Sanders", "Sanders", "Sanders"]
}, {
	"firstNames": ["Ta"],
	"lastNames": ["Frese"]
}, {
	"firstNames": ["Nora"],
	"lastNames": ["Selander"]
},{
	"firstNames": ["Kayla"],
	"lastNames": ["Go"]
}, {
	"firstNames": ["Alicia"],
	"lastNames": ["Lewis"]
}, {
	"firstNames": ["Rachael"],
	"lastNames": ["Riggle"]
}, {
	"firstNames": ["Ivan"],
	"lastNames": ["Bernal"]
}, {
	"firstNames": ["Solomon"],
	"lastNames": ["Lowe"]
}, {
	"firstNames": ["Matt"],
	"lastNames": ["Ferris"]
}, {
	"firstNames": ["David"],
	"lastNames": ["White"]
}, {
	"firstNames": ["Callum"],
	"lastNames": ["Crist"]
}, {
	"firstNames": [
		"Luke", "Haley"
	],
	"lastNames": ["Westbrook", "McLendon"]
}, {
	"firstNames": [
		"Dan", "Kate", "Emma", "Jolie"
	],
	"lastNames": ["Mills", "Mills", "Mills", "Mills"]
}, {
	"firstNames": ["Muriah"],
	"lastNames": ["Carlson"]
}, {
	"firstNames": [
		"Rex", "Joanne", "Jenna"
	],
	"lastNames": ["Dudley", "Dudley", "Jenna"]
}, {
	"firstNames": [
		"Jen", "Corbin", "McKinley", "Cole"
	],
	"lastNames": ["Anderson", "Anderson", "Anderson", "Anderson"]
}, {
	"firstNames": [
		"Kelsey", "Peyton"
	],
	"lastNames": ["Wiederspan", "Wiederspan"]
}, {
	"firstNames": [
		"Brian", "Briana"
	],
	"lastNames": ["Kerrick", "Yamasaki"]
}, {
	"firstNames": [
		"David", "Shauna", "Samantha", "Alexandria"
	],
	"lastNames": ["Ash", "Madden", "Ash", "Ash"]
}, {
	"firstNames": ["Dorothy"],
	"lastNames": ["Allen"]
}, {
	"firstNames": [
		"Elliott", "Eboney"
	],
	"lastNames": ["Allen", "Jackson"]
}, {
	"firstNames": [
		"Gary", "Fiona"
	],
	"lastNames": ["Allen", "Allen"]
}, {
	"firstNames": [
		"Judy", "Walter"
	],
	"lastNames": ["Ash", "Ash"]
}, {
	"firstNames": ["Connie"],
	"lastNames": ["Ely"]
}, {
	"firstNames": [
		"Teresa", "Mike", "MacKenzie", "Cooper", "Tatum"
	],
	"lastNames": ["Martin", "Martin", "Martin", "Martin", "Martin"]
}, {
	"firstNames": [
		"Daniel", "Lindsey", "Lauren"
	],
	"lastNames": ["Allen", "Allen", "Allen"]
}, {
	"firstNames": [
		"Mindy", "Dan", "Connor", "Quinn"
	],
	"lastNames": ["Broman", "Broman", "Broman", "Broman"]
}, {
	"firstNames": ["Patrick"],
	"lastNames": ["Allen"]
}, {
	"firstNames": [
		"Kelly", "Jay", "Caitlyn", "Makena"
	],
	"lastNames": ["Harris", "Harris", "Allen", "Allen"]
}, {
	"firstNames": [
		"Jill", "Dane", "Brooke"
	],
	"lastNames": ["Halvorson", "Halvorson", "Halvorson"]
}, {
	"firstNames": ["Drew"],
	"lastNames": ["Halvorson"]
}, {
	"firstNames": [
		"Cheryl", "Mike", "Piper", "Rainy"
	],
	"lastNames": ["Westrom", "Westrom", "Westrom", "Westrom"]
}, {
	"firstNames": ["Madie"],
	"lastNames": ["Westrom"]
}, {
	"firstNames": [
		"Cindy", "Leon"
	],
	"lastNames": ["Lucchesi", "Gratto"]
}, {
	"firstNames": [
		"Pat", "Lori"
	],
	"lastNames": ["Sokolan", "Sokolan"]
}, {
	"firstNames": ["Jackie"],
	"lastNames": ["Stevenson"]
}, {
	"firstNames": [
		"Angelica", "Alex", "Ori", "Teddy"
	],
	"lastNames": ["Weenick", "Weenick", "Weenick", "Weenick"]
}, {
	"firstNames": [
		"Sam", "Joe"
	],
	"lastNames": ["Ash", "Ash"]
}, {
	"firstNames": [
		"Nathan"
	],
	"lastNames": ["Davenport"]
}];

    $('#rsvp-form').submit(function() {
        if ($('#rsvp-form-submit-btn').attr('value') == "RSVP") {
            var action = $(this).attr('action'),
                events = $('#rsvp-form-events').val(),
                events2 = $('#rsvp-form-events2').val();
            console.log(events)
            console.log(events2)
            if (events === null) {
                events = new Array();
            }
            if (events2 === null) {
                events2 = new Array();
            }
            var firstName = $('#rsvp-form-name').val()
            var lastName = $('#rsvp-form-name2').val()
            $('#rsvp-form-submit-btn').attr('disabled', 'disabled').css('opacity', .5);
            $.post('php/rsvp.php', {
                name: firstName + " " + lastName,
                email: $('#rsvp-form-email').val(),
                going: $.map(events, function(elem) {
                    return $('#rsvp-form-events option[value=' + elem + ']').html();
                }),
                notgoing: $.map(events2, function(elem) {
                    return $('#rsvp-form-events2 option[value=' + elem + ']').html();
                })
            }, function(data) {
                $('#message2').html(data);
                $('#rsvp-form-submit-btn').removeAttr('disabled').css('opacity', 1);
                if (data.match('Thank you') !== null)
                    $('#rsvp-form').slideUp('fast', function() {
                        $(window).resize();
                    });
                }
            );
        } else {
            var firstName = $('#rsvp-form-name').val().toLowerCase();
            var lastName = $('#rsvp-form-name2').val().toLowerCase();
            var guest = -1;
            for (var i = 0; i < guests.length; i++) {
                for (var j = 0; j < guests[i]["firstNames"].length; j++) {
                    if (guests[i]["firstNames"][j].toLowerCase() == firstName && guests[i]["lastNames"][j].toLowerCase() == lastName) {
                        guest = i
                    }
                }
            }
            var invite = '<li class="flex-responsive" data-event="' +
            "brandon-fox" +
            '"><img class="img-as-bg" src="img/placeholders/800x800.png" alt=""><p>' +
            "Brandon Fox" +
            '</p></li>'

            if (guest > -1) {
                var guestList = ""
                $("#message").html("Click to place a check next to the guests if you are planning on attending.")

                var finalInvite = '<ul class="events">'
                for (var k = 0; k < guests[guest]["firstNames"].length; k++) {
                    guestList += guests[guest]["firstNames"][k] + " " + guests[guest]["lastNames"][k]
                    finalInvite += makeInvite(guests[guest]["firstNames"][k], guests[guest]["lastNames"][k], 'img/' + (k + 1) + '.jpeg')
                }
                finalInvite += '</ul>'
                finalInvite += '<select multiple name="goingevents" id="rsvp-form-events">'
                for (var k = 0; k < guests[guest]["firstNames"].length; k++) {
                    finalInvite += makeSelect(guests[guest]["firstNames"][k], guests[guest]["lastNames"][k])
                }
                finalInvite += '</select>'
                finalInvite += '<select multiple selected name="notgoingevents" id="rsvp-form-events2">'
                for (var k = 0; k < guests[guest]["firstNames"].length; k++) {
                    finalInvite += makeSelected(guests[guest]["firstNames"][k], guests[guest]["lastNames"][k])
                }
                finalInvite += '</select>'

                $("#invites").html(finalInvite)
                $('.img-as-bg2').each(function(index, el) {
                    var $img = $(el);

                    $img.parent().css('background-image', 'url("' + $img.attr('src') + '")');
                    $img.remove();
                });
                $('.section-rsvp ul.events>li').on('click', function() {
                    var $this = $(this),
                        event = $this.data('event'),
                        $options = $('form.rsvp option[value=' + event + ']'),
                        $option1 = $options.first(),
                        $option2 = $options.last();
                    if ($this.hasClass('selected')) {
                        $option1.prop('selected', false);
                        $option2.prop('selected', true);

                    } else {
                        $option1.prop('selected', true);
                        $option2.prop('selected', false);

                    }
                    $this.removeClass('clicked');
                    $this.toggleClass('selected');
                });
                $('#rsvp-form-submit-btn').attr('value', "RSVP")
            } else {
                $("#message").html("We couldn't seem to find your invite, make sure you spelled your name correctly.")
            }
        }

        return false;
    });

    function makeSlug(firstName, lastName) {
        return firstName.toLowerCase() + "-" + lastName.toLowerCase()
    }

    function makeSelect(firstName, lastName) {
        return '<option value="' + makeSlug(firstName, lastName) + '">' + firstName + " " + lastName + '</option>'
    }

    function makeSelected(firstName, lastName) {
        return '<option selected value="' + makeSlug(firstName, lastName) + '">' + firstName + " " + lastName + '</option>'
    }

    function makeInvite(firstName, lastName, img) {
        return '<li class="flex-responsive clicked" style="height:100px;" data-event="' + makeSlug(firstName, lastName) + '"><img class="img-as-bg2" src="' + img + '" alt=""><p>' + firstName + " " + lastName + '</p></li>'
    }

});
